// webpack.config.js
module.exports = {
  entry: './src/main.js',
  output: {
      filename: 'bundle.js'
  },
  devtool: 'eval-source-map',
  module: {
      loaders: [
          {
              loader: 'babel-loader',
              test: /\.js$/,
              exclude: /node_modules/
          }
      ]
  },
  devServer: {
      port: 3000
  }
}